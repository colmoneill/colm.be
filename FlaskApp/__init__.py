from flask import Flask, render_template, redirect
import feedparser, datetime
from operator import itemgetter
import urllib3
urllib3.disable_warnings()
from bs4 import BeautifulSoup
import re
from time import mktime
from datetime import datetime

# from feeds import commits

app = Flask(__name__)

from apscheduler.schedulers.background import BackgroundScheduler

def getGitFeeds():
    with app.app_context():
        gitlab = feedparser.parse('https://gitlab.com/colmoneill.atom?feed_token=bRpn4pzyps8freTKJTxd')
        # gitconstant = feedparser.parse('https://gitlab.constantvzw.org/colm.atom')
        # github = feedparser.parse('https://github.com/colmoneill.atom')
        codeberg = feedparser.parse('https://codeberg.org/ColmONeill.rss')
        # codeberg = codeberg.entries[2]
        #print(codeberg)
        gitlab = gitlab.entries[:30]
        # gitconstant = gitconstant.entries[:10]
        # github = github.entries[:10]
        global gitlabresearch
        gitlabresearch = feedparser.parse('https://gitlab.com/colmoneill/social-justice-and-education/-/commits/main.atom')
        gitlabresearch = gitlabresearch.entries[:20]
        global commits 
        commits = gitlab 
        commits = sorted(commits, key=itemgetter('updated'), reverse=True)
        commits = commits
        for entry in commits:
            entry.commit = entry.summary
            soup = BeautifulSoup(entry.commit, "html.parser")
            soup = soup.find("p", dir="auto")
            entry.commit = soup
            entry.hreflink = entry.link
            entry.link = entry.link.split('/-/')[0]
            entry.projectname = entry.link.split('/')[-1]
            #entry.title = entry.title.split(' ', 1)[1]
            #entry.reponame = entry.title.split()
            #entry.reponame = entry.reponame[-1]
            entry.updated = datetime.fromtimestamp(mktime(entry.updated_parsed))

getGitFeeds()
print ("all feed fetch tasks finished")
apsched = BackgroundScheduler()
apsched.start()
apsched.add_job(getGitFeeds, 'interval', minutes=1)

@app.route("/")
def home():
    for item in commits:
        commit_html = item['summary_detail']['value']
        soup = BeautifulSoup(commit_html, "html.parser")
        #print(soup.prettify())
        commit = soup.div
    return render_template("home.html", commits=commits)

@app.route("/cv")
def cv():
    return render_template("cv.html")

@app.route("/research")
def research():
    research = gitlabresearch
    return render_template("research.html", gitlabresearch=gitlabresearch)

@app.route("/piwik")
def piwik():
    return redirect("../../../../colmoneill.net/piwik/index.php", code=302)

@app.route("/samples-of-work")
def samples():
    return render_template("samples-of-work.html")

@app.route("/education")
def education():
    return render_template("education.html")

@app.errorhandler(404)
def not_found(error):
    return render_template('error.html'), 404

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
