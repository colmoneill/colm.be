const gulp = require('gulp');
const concatCss = require('gulp-concat-css');
const compass = require('gulp-compass');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const uglify = require('gulp-uglify');
const gulpIf = require('gulp-if');
const pump = require('pump');
const { exec } = require('child_process');
const browserSync = require('browser-sync').create();

// Task to activate the virtual environment and run Flask
function runFlask(cb) {
    exec('. venv/bin/activate && python __init__.py', (err, stdout, stderr) => {
        console.log(stdout);
        console.error(stderr);
        cb(err);
    });
}

// Task to process CSS
function processCss() {
    return gulp.src('./static/css/**/*.scss')
        .pipe(compass({
            config_file: './static/config.rb',
            css: './static/build',
            sass: './static/css'
        }))
        .on('error', function (error) {
            console.log(error);
        })
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({ compatibility: 'ie8' }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('./static/build/'))
        .pipe(browserSync.stream()); // Notify BrowserSync about changes in CSS files
}

// Task to compress JS
function compressJs(cb) {
    pump([
        gulp.src('./static/js/*.js'),
        uglify(),
        gulp.dest('./static/build/')
    ],
        cb
    );
}

// Task to initialize BrowserSync
function initBrowserSync(cb) {
    browserSync.init({
        proxy: 'localhost:5000',
        open: false, // Prevents BrowserSync from opening a new browser window
        port: 7000, // You can change the port number if needed
    });
    cb();
}

// Watch task for BrowserSync
function watch(cb) {
    gulp.watch(['./static/css/**/*.scss', './static/js/*.js']).on('change', gulp.series(processCss, browserSync.reload));
    cb();
}

// Default task
exports.default = gulp.parallel(runFlask, processCss, compressJs, initBrowserSync, watch);
