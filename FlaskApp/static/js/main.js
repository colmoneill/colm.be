document.addEventListener("DOMContentLoaded", function() {
  console.log("document loaded");
  $('.toggle-visible').click(function () {
        console.log(this);
        $(this).removeAttr("href");
        $(this).next('div.commit-content').slideToggle();
    }
  );
  $('.more-commits').click(function (){
    $(this).removeAttr("href");
    $('div.more-git-commits').slideToggle();
  });
  $(".more-commits").click(function() {
    if ($(this).text() == "↓ more commits")
    {
      $(this).text("↑ less commits");
    }
    else
    {
      $(this).text("↓ more commits");
    };
  });
  $('#about').click(function (){
    $(this).removeAttr("href");
    $('div.about').slideToggle();
  });


});
