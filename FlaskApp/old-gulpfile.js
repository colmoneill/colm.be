var gulp = require('gulp');
var concatCss = require('gulp-concat-css');
var compass = require('gulp-compass');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var pump = require('pump');

gulp.task('css', function () {
   gulp.src('./static/css/**/*.scss')
        .pipe( compass({
          config_file: './static/config.rb',
          css: './static/build/',
          sass: './static/css'
        }))
        .on('error', function(error) {
          console.log(error);
        })
        .pipe(sourcemaps.init())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(sourcemaps.write())
        .pipe( gulp.dest('./static/build/') );
});

gulp.task('compress', function (cb) {
  pump([
        gulp.src('./static/js/*.js'),
        uglify(),
        gulp.dest('./static/build/')
    ],
    cb
  );
});
gulp.task('watch', function () {
    gulp.watch(['./static/**/*.scss', './static/js/*.js'], ['css', 'compress']);
});

gulp.task('default', [ 'css', 'compress' ]);
