# Personal website colm.be

## Flask & back-end stuff

```
$ virtualenv venv
$ . venv/bin/activate
$ pip install -R requirements.txt
$ python __init__.py
```
 
## CSS only

using Compass and Sass for front end

```
$ compass watch
```

## mega excessive powerup turbo all task with Gulp js

```
$ npm install
$ gulp
```

→ which starts the `venv`, 
→ then runs the python line to start flask, 
→ then processes the `scss` into `css`
→ then compresses the `js`
→ then starts browserSync on localhost:7000

<localhost:7000>
